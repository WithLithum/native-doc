# _LOG10

## Definition

Returns the base 10 logarithm of a specified number.

!!! warning
    The name of this native functions starts with an underscore, which means
    that the current name does not match the original hash of this native
    function, and is subject to change.

=== "C/C++"

    ```c++
    float _LOG10(float value);
    ```

=== "C#"

    ```csharp
    public static float Log10(float value);
    ```

=== "JavaScript"

    ```js
    function log10(float value);
    ```

=== "Java"

    ```java
    public static float log10(float value);
    ```

### Parameters

* `value`: Single
  
  The value to calculate.

### Returns

??? info "Testing needed"
    What happens if `NaN`, `0`, negative values, or positive infinity passed
    into it? RPH, Cfx.re or SHV must be used, as they are not seen casting
    values to `ulong` (doing so throws an exception on CLR).

The base 10 logarithm of the specified number.

# TO_FLOAT

## Definition

Converts a 32-bit integer to the corresponding integral value as a
single-precision floating-point value.

=== "C/C++"

    ```c++
    float TO_FLOAT(int x);
    ```

=== "C#"

    ```csharp
    public static float ToFloat(int x);
    ```

=== "JavaScript"

    ```js
    function to_float(int x);
    ```

=== "Java"

    ```java
    public static float to_float(int x);
    ```

### Parameters

* `x`: Int32
  
  A 32-bit integer to be converted.

### Returns

The corresponding integral value of `x` as a single-precision floating-point
value.

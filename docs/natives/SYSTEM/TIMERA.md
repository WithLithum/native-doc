# TIMERA

## Definition

Gets the value of the first game engine timer.

=== "C/C++"

    ```c++
    int TIMERA();
    ```

=== "C#"

    ```csharp
    public static int TimerA();
    ```

=== "JavaScript"

    ```js
    function timera();
    ```

=== "Java"

    ```java
    public static int timerA();
    ```

### Returns

**Int32**: The current value of the timer.

## Remarks

To properly use the game engine timer it must be set first via [SET_TIMERA](../SET_TIMERA)
native function.

## See Also

* [SET_TIMERA](../SET_TIMERA)
* [SET_TIMERB](../SET_TIMERB)
* [TIMERB](../TIMERB)

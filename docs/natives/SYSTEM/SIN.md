# SIN

## Definition

Returns the sine of the specified angle.

=== "C/C++"

    ```c++
    float SIN(float value);
    ```

=== "C#"

    ```csharp
    public static float Sin(float value);
    ```

=== "JavaScript"

    ```js
    function sin(float value);
    ```

=== "Java"

    ```java
    public static float sin(float value);
    ```

### Parameters

* `value`: Single
  
  The value to calculate.

### Returns

The sine of the specified angle.

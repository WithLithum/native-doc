# COS

## Definition

Returns the cosine of the specified angle.

=== "C/C++"

    ```c++
    float COS(float value);
    ```

=== "C#"

    ```csharp
    public static float Cos(float value);
    ```

=== "JavaScript"

    ```js
    function cos(float value);
    ```

=== "Java"

    ```java
    public static float cos(float value);
    ```

### Parameters

* `value`: Single
  
  The value to calculate.

### Returns

The cosine of the specified angle.

# START_NEW_SCRIPT_WITH_NAME_HASH

## Definition

Starts a new native script registered in the script registry, with the name
specified as a hash.

=== "C/C++"

    ```c++
    int START_NEW_SCRIPT(Hash scriptName, int stackSize);
    ```

=== "C#"

    ```csharp
    public static int StartNewScript(uint hash, int stackSize);
    ```

=== "JavaScript"

    ```js
    function start_new_script(long hash, int stackSize);
    ```

=== "Java"

    ```java
    public static int startNewScript(long hash, int stackSize);
    ```

### Parameters

* `scriptName`: Hash
  
  The name of the script to start.

* `stack`: Int32
  
  The size of the stack.

### Returns

**Int32**: The handle of the started script.

## Examples

The following example starts a new script called `AM_MP_YACHT`:

=== "C/C++"

    ```c++
    SYSTEM::START_NEW_SCRIPT("AM_MP_YACHT", 5000);
    ```

=== "C# (RPH)"

    ```csharp
    NativeFunction.Natives.START_NEW_SCRIPT<int>("AM_MP_YACHT", 5000);
    ```

=== "C# (SHVDN)"

    ```csharp
    Function.Call<int>(Hash.START_NEW_SCRIPT, "AM_MP_YACHT", 5000);
    ```

=== "JavaScript"

    ```javascript
    start_new_script("AM_MP_YACHT", 5000);
    ```
=== "Java"

    ```java
    System.startNewScript("AM_MP_YACHT", 5000);
    ```

## Remarks

This script may only start registered native scripts. For other type of the
scripts, use the functions from the hook to start them; if you need more
information you can consult the hook documentation.

# Summary

The `SYSTEM` namespace provides threading, calculation, and language utility
for native scripts.

Normal scripts like ASI and .NET scripts do not need to use these and may not
work very well with these, unless you need to work with native scripts or
threads (which needs to use a few of the natives in this namespace).

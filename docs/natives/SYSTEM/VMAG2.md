# VMAG2

## Definition

Calculates the magnitude of a three-dimensional vector without performing
a square root calculation.

**Overload of** [VMAG](../VMAG)

=== "C/C++"

    ```c++
    float VMAG2(float x, float y, float z);
    ```

=== "C#"

    ```csharp
    public static float Vmag2(float x, float y, float z);
    ```

=== "JavaScript"

    ```js
    function vmag(float x, float y, float z);
    ```

=== "Java"

    ```java
    public static float vmag(float x, float y, float z);
    ```

### Parameters

* `x`: Single
  
  The X component of the vector.

* `y`: Single
  
  The Y component of the vector.

* `z`: Single
  
  The Z component of the vector.

### Returns

The magnitude of the specified vector.

## Remarks

It is recommended to just use the relevant calculation method on the `Vector3`
type of the API for your hook if available as it does direct calculation (or
calculated beforehand and stored in a readonly/final value) and is much
faster and easier than calling a native to do so.

## See Also

* [VMAG](../VMAG)

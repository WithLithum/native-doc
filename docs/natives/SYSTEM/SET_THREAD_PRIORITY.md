# SET_THREAD_PRIORITY

## Definition

Sets the priority of the current native thread.

=== "C/C++"

    ```c
    void SET_THREAD_PRIORITY(int priority);
    ```

=== "C#"

    ```csharp
    public static void SetThreadPriority(NativeThreadPriority priority);
    ```

=== "JavaScript"

    ```js
    function wait(int priority);
    ```

=== "Java"

    ```java
    public static void wait(NativeThreadPriority priority);    
    ```

### Parameters

* `priority`: NativeThreadPriority (Int32)
  
  The priority to set to.

## Remarks

This only works for native threads. For CLR threads, use the CLR classes to do
so.

## Structures

The native thread priority is an enumeration of the following values:

=== "C/C++"

    ```c
    enum NativeThreadPriority {
        High,
        Normal,
        Low
    };
    ```

=== "C#"

    ```csharp
    public enum NativeThreadPriority {
        High,
        Normal,
        Low
    }
    ```

=== "JavaScript"

    ```js
    enum NativeThreadPriority {
        high,
        normal,
        low
    }
    ```

=== "Java"

    ```java
    public enum NativeThreadPriority {
        HIGH,
        NORMAL,
        LOW
    }    
    ```

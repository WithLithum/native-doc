# CEIL

## Definition

Returns the smallest integral value that is greater than or equal to the
specified single-precision floating-point number.

=== "C/C++"

    ```c++
    int CEIL(float x);
    ```

=== "C#"

    ```csharp
    public static int Ceil(float x);
    ```

=== "JavaScript"

    ```js
    function ceil(float x);
    ```

=== "Java"

    ```java
    public static int ceil(float x);
    ```

### Parameters

* `value`: Single
  
  A single-precision floating-point number.

### Returns

??? note "Test required"
    What if `x` is equal to `NaN`, `NegativeInfinity`, or `PositiveInfinity`?

The largest integral value less than or equal to `x`.

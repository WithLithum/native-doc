# ROUND

## Definition

Rounds a single-precision floating-point value to the nearest integral value,
and rounds midpoint values to the nearest even number.

=== "C/C++"

    ```c++
    int ROUND(float x);
    ```

=== "C#"

    ```csharp
    public static int Round(float x);
    ```

=== "JavaScript"

    ```js
    function round(float x);
    ```

=== "Java"

    ```java
    public static int round(float x);
    ```

### Parameters

* `value`: Single
  
  A single-precision floating-point number to be rounded.

### Returns

??? note "Test required"
    What if the fractional component of `x` is halfway between
    two integers, one of which is even and the other odd?

The integer nearest `x`.

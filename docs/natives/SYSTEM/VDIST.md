# VDIST

## Definition

Calculates the distance between three-dimensional vectors.

=== "C/C++"

    ```c++
    float VDIST(float x, float y, float z, float x2, float y2, float z2);
    ```

=== "C#"

    ```csharp
    public static float VDist(float x, float y, float z, float x2, float y2, float z2);
    ```

=== "JavaScript"

    ```js
    function vdist(float x, float y, float z, float x2, float y2, float z2);
    ```

=== "Java"

    ```java
    public static float vDist(float x, float y, float z, float x2, float y2, float z2);
    ```

### Parameters

* `x`: Single
  
  The X component of the first vector.

* `y`: Single
  
  The Y component of the first vector.

* `z`: Single
  
  The Z component of the first vector.

* `x2`: Single
  
  The X component of the second vector.

* `y2`: Single
  
  The Y component of the second vector.

* `z2`: Single

  The Z component of the second vector.

### Returns

The distance between two specified vectors.

## Remarks

!!! note
    This native performs square root calculation which may slow down the speed
    of the calculation. If you need quicker calculation, use [VDIST2](../VDIST2)
    instead.

It is recommended to just use the relevant calculation method on the `Vector3`
type of the API for your hook if available as it does direct calculation and is
much faster and easier than calling a native to do so.

## See Also

* [VDIST2](../VDIST2)

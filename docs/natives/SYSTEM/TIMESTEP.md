# TIMESTEP

## Definition

Gets the current frame time.

=== "C/C++"

    ```c++
    int TIMESTEP();
    ```

=== "C#"

    ```csharp
    public static int Timestep();
    ```

=== "JavaScript"

    ```js
    function timestep();
    ```

=== "Java"

    ```java
    public static int timestep();
    ```

### Returns

**Int32**: The current frame time.

# SHIFT_LEFT

## Definition

Performs a left bit-shift calculation.

!!! warning
    This native function should not be used as it should be a standard language
    feature in most of the programming languages, and this native is the same -
    it is a basic language feature of the YSC language.

=== "C/C++"

    ```c++
    float SHIFT_LEFT(int value, int shift);
    ```

=== "C#"

    ```csharp
    public static float ShiftLeft(int value, int shift);
    ```

=== "JavaScript"

    ```js
    function shift_left(int value, int shift);
    ```

=== "Java"

    ```java
    public static float shiftLeft(int value, int shift);
    ```

### Parameters

* `value`: Int32
  
  The value to shift.

* `shift`: Int32
  
  The shift.

### Returns

The value of `value` left-shifted by `shift`.

## Remarks

It is recommended to just use the `<<` operator instead of calling this native
as it is much faster.

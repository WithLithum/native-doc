# POW

## Definition

Returns a specified number raised to the specified power.

=== "C/C++"

    ```c++
    float POW(float x, float y);
    ```

=== "C#"

    ```csharp
    public static float Pow(float x, float y);
    ```

=== "JavaScript"

    ```js
    function pow(float x, float y);
    ```

=== "Java"

    ```java
    public static float pow(float x, float y);
    ```

### Parameters

* `x`: Single
  
  A single-precision floating-point number to be raised to a power.

* `y`: Single
  
  A single-precision floating-point number that specifies a power.

### Returns

Single: The number `x` raised to the power `y`.

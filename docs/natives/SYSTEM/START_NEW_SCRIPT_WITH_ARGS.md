# START_NEW_SCRIPT_WITH_ARGS

## Definition

Starts a new native script registered in the script registry, with the
specified arguments.

=== "C/C++"

    ```c++
    int START_NEW_SCRIPT(const char* scriptName, Any* args, int argCount, int stackSize);
    ```

=== "C#"

    ```csharp
    public static int StartNewScript(string scriptName, object[] args, int argCount, int stackSize);
    ```

=== "JavaScript"

    ```js
    function start_new_script(string scriptName, object[] args, int argCount int stackSize);
    ```

=== "Java"

    ```java
    public static int startNewScript(String scriptName, Object[] args, int argCount, int stackSize);
    ```

### Parameters

* `scriptName`: String
  
  The name of the script to start.

* `args`: Object[]
  
  An array of arguments.

* `argCount`: Int32

  The amount of the arguments in `args`.

* `stack`: Int32
  
  The size of the stack.

### Returns

**Int32**: If success, the handle of the started script; otherwise, `0`.

## Remarks

This script may only start registered native scripts. For other type of the
scripts, use the functions from the hook to start them; if you need more
information you can consult the hook documentation.

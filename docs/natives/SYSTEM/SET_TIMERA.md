# SET_TIMERA

## Definition

Sets the value of the first game engine timer.

=== "C/C++"

    ```c++
    void SET_TIMERA(int value);
    ```

=== "C#"

    ```csharp
    public static void SetTimerA(int value);
    ```

=== "JavaScript"

    ```js
    function set_timera(int value);
    ```

=== "Java"

    ```java
    public static void setTimerA(int value);
    ```

### Parameters

* `value`: Int32
  
  The value to set.

## See Also

* [SET_TIMERB](../SET_TIMERB)
* [TIMERA](../TIMERA)
* [TIMERB](../TIMERB)

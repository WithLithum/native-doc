# FLOOR

## Definition

Returns the largest integral value less than or equal to the specified
single-precision floating-point number.

=== "C/C++"

    ```c++
    int FLOOR(float x);
    ```

=== "C#"

    ```csharp
    public static int Floor(float x);
    ```

=== "JavaScript"

    ```js
    function floor(float x);
    ```

=== "Java"

    ```java
    public static int floor(float x);
    ```

### Parameters

* `value`: Single
  
  A single-precision floating-point number.

### Returns

??? note "Test required"
    What if `x` is equal to `NaN`, `NegativeInfinity`, or `PositiveInfinity`?

The largest integral value less than or equal to `x`.

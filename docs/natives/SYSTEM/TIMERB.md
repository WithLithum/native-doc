# TIMERB

## Definition

Gets the value of the second game engine timer.

=== "C/C++"

    ```c++
    int TIMERB();
    ```

=== "C#"

    ```csharp
    public static int TimerB();
    ```

=== "JavaScript"

    ```js
    function timerb();
    ```

=== "Java"

    ```java
    public static int timerB();
    ```

### Returns

**Int32**: The current value of the timer.

## Remarks

To properly use the game engine timer it must be set first via [SET_TIMERB](../SET_TIMERB)
native function.

## See Also

* [SET_TIMERA](../SET_TIMERA)
* [SET_TIMERB](../SET_TIMERB)
* [TIMERA](../TIMERA)

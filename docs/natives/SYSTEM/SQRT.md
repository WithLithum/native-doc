# SQRT

## Definition

Returns the square root of the specified number.

=== "C/C++"

    ```c++
    float SQRT(float value);
    ```

=== "C#"

    ```csharp
    public static float Sqrt(float value);
    ```

=== "JavaScript"

    ```js
    function sqrt(float value);
    ```

=== "Java"

    ```java
    public static float sqrt(float value);
    ```

### Parameters

* `value`: Single
  
  The value to calculate.

### Returns

The square root of the specified number.

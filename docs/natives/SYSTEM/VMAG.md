# VMAG

## Definition

Calculates the magnitude of a three-dimensional vector.

=== "C/C++"

    ```c++
    float VMAG(float x, float y, float z);
    ```

=== "C#"

    ```csharp
    public static float VMag(float x, float y, float z);
    ```

=== "JavaScript"

    ```js
    function vmag(float x, float y, float z);
    ```

=== "Java"

    ```java
    public static float vMag(float x, float y, float z);
    ```

### Parameters

* `x`: Single
  
  The X component of the vector.

* `y`: Single
  
  The Y component of the vector.

* `z`: Single
  
  The Z component of the vector.

### Returns

The magnitude of the specified vector.

## Remarks

!!! note
    This native performs square root calculation which may slow down the speed
    of the calculation. If you need quicker calculation, use [VMAG2](../SYSTEM/VMAG2.md)
    instead.

It is recommended to just use the relevant calculation method on the `Vector3`
type of the API for your hook if available as it does direct calculation and is
much faster and easier than calling a native to do so.

## See Also

* [VMAG2](../VMAG2)

# SET_TIMERB

## Definition

Sets the value of the second game engine timer.

=== "C/C++"

    ```c++
    void SET_TIMERB(int value);
    ```

=== "C#"

    ```csharp
    public static void SetTimerB(int value);
    ```

=== "JavaScript"

    ```js
    function set_timerb(int value);
    ```

=== "Java"

    ```java
    public static void setTimerB(int value);
    ```

### Parameters

* `value`: Int32
  
  The value to set.

## See Also

* [SET_TIMERA](../SET_TIMERA)
* [TIMERA](../TIMERA)
* [TIMERB](../TIMERB)

# WAIT

Pauses the execution of this native script for the specified milliseconds.

=== "C/C++"

    ```c
    void WAIT(int time);
    ```

=== "C#"

    ```csharp
    public static void Wait(int time);
    ```

=== "JavaScript"

    ```js
    function wait(int time);
    ```

=== "Java"

    ```java
    public static void wait(int time);
    ```

## Parameters

* `time`: Int32
  
  The time as milliseconds to pause the execution.

## Remarks

The `WAIT` function may not work for scripts other than standard native
scripts. Non-native scripts should consult their hook/API documentation for
how to pause the execution of their user scripts.

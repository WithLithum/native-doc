# _SET_PED_CAN_PLAY_INJURED_ANIMS

## Definition

Sets a value indicating whether the specified ped can play injured animations.

!!! warning
    The name of this native functions starts with an underscore, which means
    that the current name either does not match the original hash of this native
    function, or there is no original hash for this native function. Thus, the name
    is subject to change.

=== "C/C++"

    ```c
    void _SET_PED_CAN_PLAY_INJURED_ANIMS(Ped ped, bool enable);
    ```

=== "C#"

    ```csharp
    public static void SetPedCanPlayInjuredAnims(Ped ped, bool enable);
    ```

=== "JavaScript"

    ```js
    function _set_ped_can_play_injured_anims(Ped ped, bool enable);
    ```

=== "Java"

    ```java
    public static void setPedCanPlayInjuredAnims(Ped ped, bool enable);
    ```

### Parameters

`ped`: Int as Ped

:   The ped.

`enable`: Boolean

:   If `true`, the ped cannot play injured animations.

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | Build 323                            |

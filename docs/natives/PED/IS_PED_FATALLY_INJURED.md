# IS_PED_FATALLY_INJURED

## Definition

Determines whether the specified instance is fatally injured, dead, or ceased
to exist.

```c title="Definition (C)"
bool IS_PED_FATALLY_INJURED(Ped ped);
```

### Parameters

`ped`: Ped as Int32

:   The ped to check.

### Returns

`true` if the specified ped was dead, health below fatally injured threshold,
or ceased to exist.

## Remarks

Peds who are fatally injured can be considered dead and should not be used,
unless you expect to use dead ped.

!!! warning
    Dead peds cannot be tasked. Make sure you check for existence because this
    native returns `true` rather than fail if an invalid handle was passed.

## Applies to

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V (NG)         | Build 323                            |
| GTA V (OG)         | Latest                               |

# GET_VEHICLE_PED_IS_USING

## Definition

Gets the vehicle that the ped is or was interacting with.

=== "C/C++"

    ```c
    Vehicle GET_VEHICLE_PED_IS_USING(Ped ped);
    ```

=== "C#"

    ```csharp
    public static uint GetVehiclePedIsUsing(uint ped);
    ```

=== "JS / TS"

    ```typescript
    function get_vehicle_ped_is_using(ped: uint): uint;
    ```

=== "Java"

    ```java
    public static Vehicle getVehiclePedIsUsing(Ped ped);
    ```

### Parameters

`ped`: Ped as UInt32

:   The ped.

### Returns

??? todo "Test required"

    What happens if the ped was not using any vehicle?

The vehicle that the ped is currently using.

## Applies to

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V (NG)         | Build 323                            |
| GTA V (OG)         | Latest                               |

# SET_PED_CAN_BE_TARGETTED

## Definition

Makes the specified ped can be targeted or not.

```c
void SET_PED_CAN_BE_TARGETTED(int ped, bool toggle);
```

### Parameters

`ped`: Ped as Int32

:    The ped to set.

`toggle`: Boolean

:    If `true`, the ped can be targeted.

## Applies to

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V (NG)         | Build 323                            |
| GTA V (OG)         | Latest                               |

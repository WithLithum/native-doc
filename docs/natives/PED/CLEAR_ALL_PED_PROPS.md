# CLEAR_ALL_PED_PROPS

## Definition

Clears all outfit props of the specified ped.

=== "C/C++"

    ```c
    void CLEAR_ALL_PED_PROPS(Ped ped);
    ```

=== "C#"

    ```csharp
    public static void ClearAllPedProps(Ped ped);
    ```

=== "JavaScript"

    ```js
    function clear_all_ped_props(Ped ped);
    ```

=== "Java"

    ```java
    public static void clearAllPedProps(Ped ped);
    ```

### Parameters

`ped`: Int as Ped

:   The ped to clear the props.

## Examples

The mission Chop removes the props of a ped like this:

=== "C/C++"

    ```c
    CLEAR_ALL_PED_PROPS(ped /* iParam0 */);
    ```

=== "C#"

    ```csharp
    ClearAllPedProps(ped);
    ```

=== "JavaScript"

    ```js
    clear_all_ped_props(ped);
    ```

=== "Java"

    ```java
    clearAllPedProps(ped);
    ```

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | Build 323                            |

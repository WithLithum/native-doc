# DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG

## Definition

Determines whether the specified shop ped has the specified apparel restriction
tag.

=== "C/C++"

    ```c
    bool DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(Hash componentHash, Hash restrictionHash, int id);
    ```

=== "C#"

    ```csharp
    public static bool DoesShopPedApparelHaveRestrictionTag(uint componentHash, uint restrictionHash, int id);
    ```

=== "JS / TS"

    ```typescript
    function does_shop_ped_apparel_have_restriction_tag(componentHash: int, restrictionHash: int, id: int);
    ```

=== "Java"

    ```java
    public static boolean doesShopPedApparelHaveRestrictionTag(long componentHash, long restrictionHash, int id);
    ```

### Parameters

`componentHash`: Hash (Unsigned Int32 as least)

:   The hash of the component to check.

`restrictionHash`: Hash (Unsigned Int32 as least)

:   The hash of the restriction tag.

`id`: Int32

:   The ID of the component to check. Seems unused.

### Returns

`true` if the restriction tag is valid for the component; otherwise, `false`.

## See Also

* [List of Ped Apparel Restriction tags](https://github.com/DurtyFree/gta-v-data-dumps/blob/master/pedApparelRestrictionTags.json)

## Applies to

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V (NG)         | Build 323                            |
| GTA V (OG)         | Latest                               |

# SET_PLAYER_INVINCIBLE

## Definition

Makes the character of the specified player invincible to damages.

```c
void SET_PLAYER_INVINCIBLE(int player, BOOL invincible);
```

### Parameters

`player`: Player as Int32

:    The player.

`invincible`: Boolean

:    If `true`, the player is invincible.

## Applies to

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V (NG)         | Build 323                            |
| GTA V (OG)         | Latest                               |

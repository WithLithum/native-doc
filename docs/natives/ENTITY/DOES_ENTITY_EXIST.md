# DOES_ENTITY_EXIST

## Definition

Determines whether the specified entity handle still represents a valid
entity in the game world.

=== "C/C++"

    ```c
    bool DOES_ENTITY_EXIST(Entity entity);
    ```

=== "C#"

    ```csharp
    public static bool DoesEntityExist(uint entity);
    ```

=== "JS / TS"

    ```typescript
    function does_entity_exist(entity: number): boolean;
    ```

=== "Java"

    ```java
    public static boolean doesEntityExist(Entity entity);
    ```

### Parameters

`entity`: Entity as UInt32

:   The entity handle to check.

### Returns

`true` if the entity handle still represents a valid entity in the game world;
otherwise, `false`.

## Remarks

This native may be used throughout scripts to validate entity handles, and in
fact sometimes checking each tick is necessary.

!!! tip

    Handles validation is only needed for once each tick and they are best
    placed before any execution. Handles are guaranteed to stay valid in the
    same tick unless deleted by your code.

## Applies to

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V (NG)         | Build 323                            |
| GTA V (OG)         | Latest                               |

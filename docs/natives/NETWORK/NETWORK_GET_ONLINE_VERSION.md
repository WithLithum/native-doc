# _NETWORK_GET_ONLINE_VERSION

## Definition

Gets the online version of this client instance.

!!! warning
    The name of this native functions starts with an underscore, which means
    that the current name either does not match the original hash of this native
    function, or there is no original hash for this native function. Thus, the name
    is subject to change.

=== "C/C++"

    ```c
    // 0xFCA9373EF340AC0A
    const char* _NETWORK_GET_ONLINE_VERSION();
    ```

=== "C#"

    ```csharp
    // 0xFCA9373EF340AC0A
    public static bool NetworkGetOnlineVersion();
    ```

=== "JavaScript"

    ```js
    // 0xFCA9373EF340AC0A
    function _network_get_online_version();
    ```

=== "Java"

    ```java
    // 0xFCA9373EF340AC0A
    public static boolean networkGetOnlineVersion();
    ```

### Returns

The online version of this instance.

## Remarks

The online version this native queries is stored in `update.rpf\common\data\version.txt`.
It evaluates this part:

```ini

[ONLINE_VERSION_NUMBER]
1.33

```

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | Build 323                            |

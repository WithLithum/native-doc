# NETWORK_IS_HOST

## Definition

Determines whether this client instance is the host of the current session.

!!! warning
    This native is a network native, it may only work in multiplayer
    environments such as FiveM. Invoking network natives in Story Mode will
    result in a crash.

=== "C/C++"

    ```c
    bool NETWORK_IS_HOST();
    ```

=== "C#"

    ```csharp
    public static bool NetworkIsHost();
    ```

=== "JavaScript"

    ```js
    function network_is_host();
    ```

=== "Java"

    ```java
    public static boolean networkIsHost();
    ```

### Returns

`true` if this client is host; otherwise, `false`.

## Remarks

The native only works for host/clients model. It will most likely always
evaluate to `false` in FiveM, especially if there is OneSync enabled.

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323, <abbr title="Does not work correctly when OneSync is active.">No OneSync</abbr>) |
| GTA V              | <abbr title="May only work for multiplayer.">Multiplayer</abbr>, Build 323  |

# Summary

!!! warning
    Usage of the network natives outside of multiplayer environment such as
    FiveM will result in game crash.

These are network natives. You cannot use them in SP, but you can use them in
FiveM.

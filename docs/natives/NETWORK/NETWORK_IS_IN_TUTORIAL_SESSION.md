# NETWORK_IS_IN_TUTORIAL_SESSION

## Definition

Determines whether this session is a tutorial session.

!!! warning
    This native is a network native, it may only work in multiplayer
    environments such as FiveM. Invoking network natives in Story Mode will
    result in a crash.

=== "C/C++"

    ```c
    bool NETWORK_IS_IN_TUTORIAL_SESSION();
    ```

=== "C#"

    ```csharp
    public static bool NetworkIsInTutorialSession();
    ```

=== "JavaScript"

    ```js
    function network_is_in_tutorial_session();
    ```

=== "Java"

    ```java
    public static boolean networkIsInTutorialSession();
    ```

### Returns

`true` if the client is in a tutorial session; otherwise, `false`.

## Remarks

The tutorial session here represents the GTA Online tutorial session. It will
most likely always evaluate to `false` in FiveM.

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | <abbr title="May only work for multiplayer.">Multiplayer</abbr>, Build 323  |

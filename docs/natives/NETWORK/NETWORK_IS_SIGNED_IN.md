# NETWORK_IS_SIGNED_IN

## Definition

Determines whether this instance has signed into a valid Rockstar Games Social
Club account.

=== "C/C++"

    ```c
    const char* NETWORK_IS_SIGNED_IN();
    ```

=== "C#"

    ```csharp
    public static bool NetworkIsSignedIn();
    ```

=== "JavaScript"

    ```js
    function network_is_signed_in();
    ```

=== "Java"

    ```java
    public static boolean networkIsSignedIn();
    ```

### Returns

`true` if this client is signed into a valid Rockstar Games Social Club account;
otherwise, `false`.

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | Build 323                            |

# SET_RANDOM_TRAINS

## Definition

Sets a value indicating whether random trains should be spawned.

=== "C/C++"

    ```c
    void SET_RANDOM_TRAINS(int enable);
    ```

=== "C#"

    ```csharp
    public static void SetRandomTrains(bool enable);
    ```

=== "JavaScript"

    ```js
    function set_random_trains(boolean enable);
    ```

=== "Java"

    ```java
    public static void setRandomTrains(boolean enable);
    ```

### Parameters

`enable`: Boolean

:   If `true`, random trains will be spawned.

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | Build 323                            |

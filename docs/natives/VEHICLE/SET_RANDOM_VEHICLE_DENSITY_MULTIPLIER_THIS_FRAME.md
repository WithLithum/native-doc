# SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME

## Definition

Applies the random vehicle density multiplier specified in the current tick.

=== "C/C++"

    ```c
    void SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(float multiplier);
    ```

=== "C#"

    ```csharp
    public static void SetRandomVehicleDensityMultiplierThisFrame(float multiplier);
    ```

=== "JavaScript"

    ```js
    function set_random_vehicle_density_multiplier_this_frame(float multiplier);
    ```

=== "Java"

    ```java
    public static void setRandomVehicleDensityMultiplierThisFrame(float multiplier);
    ```

### Parameters

`multiplier`: Single

:   The multiplier.

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | Build 323                            |

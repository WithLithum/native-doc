# DELETE_ALL_TRAINS

## Definition

Deletes all trains currently existing in the game world.

=== "C/C++"

    ```c
    void DELETE_ALL_TRAINS();
    ```

=== "C#"

    ```csharp
    void DeleteAllTrains();
    ```

=== "JavaScript"

    ```js
    function delete_all_trains();
    ```

=== "Java"

    ```java
    public static void deleteAllTrains();
    ```

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | Build 323                            |

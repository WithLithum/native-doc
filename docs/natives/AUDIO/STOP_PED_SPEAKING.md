# STOP_PED_SPEAKING

## Definition

Interrupt the current speech of the specified ped.

=== "C/C++"

    ```c
    void STOP_PED_SPEAKING(Ped ped, bool shaking);
    ```

=== "C#"

    ```csharp
    // 0xFCA9373EF340AC0A
    public static void StopPedSpeaking(Ped ped, bool shaking);
    ```

=== "JavaScript"

    ```js
    // 0xFCA9373EF340AC0A
    function stop_ped_speaking(Ped ped, bool shaking);
    ```

=== "Java"

    ```java
    // 0xFCA9373EF340AC0A
    public static boolean stopPedSpeaking(Ped ped, boolean shaking);
    ```

### Parameters

`ped`: Int as Ped

:   The ped.

`shaking`: Boolean

:   Whether to stop in a hurry.

## Applies To

| Platform           | Version                              |
| ------------------ | ------------------------------------ |
| CitizenFX.re       | Client (GTAV, b323)                  |
| GTA V              | Build 323                            |
